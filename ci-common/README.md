# These scripts in this folder are deprecated

These scripts were used at one time to provide CI for a simulation platform based on Vagrant/libvirt using gitlab.  

Managing a simulation server and all of the dependencies for an in-house simulation plaform is....a pain.  

AIR is a simulation platform that powers the cumulus in the cloud envrionment https://air.cumulusnetworks.com  

AIR is a resful API based simulation platform thats hosted in the cloud. AIR has become our simulation platform for gitlab CI in the golden turtle, Production Ready Automation (PRA) project. 

These scripts may still be useful for various purposes for power users and others still using vagrant/libvirt instead of AIR, but are no longer supported and may be removed from the project at some point in the future.

Visit: https://gitlab.com/cumulus-consulting/goldenturtle/cumulus_ansible_modules for all of the current CI action

